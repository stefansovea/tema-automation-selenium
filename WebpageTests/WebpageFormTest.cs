﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace WebpageTests
{
    public class WebpageFormTest
    {
        const string homeURL = "https://www.techlistic.com/p/selenium-practice-form.html";
        const string homeTitle = "Selenium Practice Form";

        [Fact]
        [Trait("Category","Smoke")]
        public void LoadApplicationPage()
        {
            using (IWebDriver driver = new ChromeDriver())
            {
                driver.Navigate().GoToUrl(homeURL);
                Assert.Equal(homeTitle, driver.Title);
                Assert.Equal(homeURL, driver.Url);
            }
        }
        [Fact]
        [Trait("Category", "Smoke")]
        public void ReloadPage()
        {
            using (IWebDriver driver = new ChromeDriver())
            {              
                driver.Navigate().GoToUrl(homeURL);
                driver.Navigate().Refresh();
                Assert.Equal(homeTitle, driver.Title);
                Assert.Equal(homeURL, driver.Url);
            }
        }

        [Fact]
        [Trait("Category", "Input")]
        public void FormSubmitTest()
        {
            using (IWebDriver driver = new ChromeDriver())
            {
                driver.Navigate().GoToUrl(homeURL);
                driver.FindElement(By.Name("firstname")).SendKeys("Steven");
                PauseHelper.Pause();
                driver.FindElement(By.Name("lastname")).SendKeys("Hawk");
                PauseHelper.Pause();
                driver.FindElement(By.Id("sex-0")).Click();
                PauseHelper.Pause();
                driver.FindElement(By.Id("exp-2")).Click();
                PauseHelper.Pause();
                driver.FindElement(By.Id("datepicker")).SendKeys("1.08.2021");
                PauseHelper.Pause();
                driver.FindElement(By.Id("profession-1")).Click();
                PauseHelper.Pause();
                driver.FindElement(By.Id("tool-1")).Click();
                PauseHelper.Pause();
                driver.FindElement(By.Id("tool-2")).Click();
                PauseHelper.Pause();
                IWebElement continentCmbBox = driver.FindElement(By.Id("continents"));
                PauseHelper.Pause();
                SelectElement contSel = new SelectElement(continentCmbBox);
                contSel.SelectByText("Europe");
                PauseHelper.Pause();
                IWebElement selComSource = driver.FindElement(By.Id("selenium_commands"));
                SelectElement selCom = new SelectElement(selComSource);
                selCom.SelectByText("Browser Commands");
                PauseHelper.Pause();
                selCom.SelectByText("Navigation Commands");
                PauseHelper.Pause();
                driver.FindElement(By.Id("submit")).Click();
                PauseHelper.Pause();
            }
        }
    }
}
